package com.hcm.module.gzfk.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 交易历史
 */
@Entity
@Table(name = "trade_log")
public class TradeLog extends KernelModel {
    private long id;
    private String openId;
    private String type;
    private String name;
    private BigDecimal price;
    private BigDecimal yk;
    private BigDecimal closePrice;
    private BigDecimal pay;
    private String xtype;
    private Timestamp createTime;
    private Integer gainPer;
    private Integer lossPer;
    private Timestamp time;
    private String tradeNo;
    private String nickName;
    private String attr0;
    private String attr1;
    private String attr2;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "yk")
    public BigDecimal getYk() {
        return yk;
    }

    public void setYk(BigDecimal yk) {
        this.yk = yk;
    }

    @Basic
    @Column(name = "close_price")
    public BigDecimal getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(BigDecimal closePrice) {
        this.closePrice = closePrice;
    }

    @Basic
    @Column(name = "pay")
    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    @Basic
    @Column(name = "xtype")
    public String getXtype() {
        return xtype;
    }

    public void setXtype(String xtype) {
        this.xtype = xtype;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "gain_per")
    public Integer getGainPer() {
        return gainPer;
    }

    public void setGainPer(Integer gainPer) {
        this.gainPer = gainPer;
    }

    @Basic
    @Column(name = "loss_per")
    public Integer getLossPer() {
        return lossPer;
    }

    public void setLossPer(Integer lossPer) {
        this.lossPer = lossPer;
    }

    @Basic
    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "trade_no")
    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    @Basic
    @Column(name = "nick_name")
    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Basic
    @Column(name = "attr0")
    public String getAttr0() {
        return attr0;
    }

    public void setAttr0(String attr0) {
        this.attr0 = attr0;
    }

    @Basic
    @Column(name = "attr1")
    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    @Basic
    @Column(name = "attr2")
    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TradeLog tradeLog = (TradeLog) o;

        if (id != tradeLog.id) return false;
        if (openId != null ? !openId.equals(tradeLog.openId) : tradeLog.openId != null) return false;
        if (type != null ? !type.equals(tradeLog.type) : tradeLog.type != null) return false;
        if (name != null ? !name.equals(tradeLog.name) : tradeLog.name != null) return false;
        if (price != null ? !price.equals(tradeLog.price) : tradeLog.price != null) return false;
        if (yk != null ? !yk.equals(tradeLog.yk) : tradeLog.yk != null) return false;
        if (closePrice != null ? !closePrice.equals(tradeLog.closePrice) : tradeLog.closePrice != null) return false;
        if (pay != null ? !pay.equals(tradeLog.pay) : tradeLog.pay != null) return false;
        if (xtype != null ? !xtype.equals(tradeLog.xtype) : tradeLog.xtype != null) return false;
        if (createTime != null ? !createTime.equals(tradeLog.createTime) : tradeLog.createTime != null) return false;
        if (gainPer != null ? !gainPer.equals(tradeLog.gainPer) : tradeLog.gainPer != null) return false;
        if (lossPer != null ? !lossPer.equals(tradeLog.lossPer) : tradeLog.lossPer != null) return false;
        if (time != null ? !time.equals(tradeLog.time) : tradeLog.time != null) return false;
        if (tradeNo != null ? !tradeNo.equals(tradeLog.tradeNo) : tradeLog.tradeNo != null) return false;
        if (nickName != null ? !nickName.equals(tradeLog.nickName) : tradeLog.nickName != null) return false;
        if (attr0 != null ? !attr0.equals(tradeLog.attr0) : tradeLog.attr0 != null) return false;
        if (attr1 != null ? !attr1.equals(tradeLog.attr1) : tradeLog.attr1 != null) return false;
        if (attr2 != null ? !attr2.equals(tradeLog.attr2) : tradeLog.attr2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (openId != null ? openId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (yk != null ? yk.hashCode() : 0);
        result = 31 * result + (closePrice != null ? closePrice.hashCode() : 0);
        result = 31 * result + (pay != null ? pay.hashCode() : 0);
        result = 31 * result + (xtype != null ? xtype.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (gainPer != null ? gainPer.hashCode() : 0);
        result = 31 * result + (lossPer != null ? lossPer.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (tradeNo != null ? tradeNo.hashCode() : 0);
        result = 31 * result + (nickName != null ? nickName.hashCode() : 0);
        result = 31 * result + (attr0 != null ? attr0.hashCode() : 0);
        result = 31 * result + (attr1 != null ? attr1.hashCode() : 0);
        result = 31 * result + (attr2 != null ? attr2.hashCode() : 0);
        return result;
    }
}
