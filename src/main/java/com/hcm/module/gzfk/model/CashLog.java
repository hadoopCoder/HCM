package com.hcm.module.gzfk.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 提现记录表
 */
@Entity
@Table(name = "cash_log")
public class CashLog extends KernelModel {
    private long id;
    private String openId;
    private String phone;
    private String weChat;
    private BigDecimal pay;
    private Timestamp createTime;
    private String status;
    private String result;
    private String flag;
    private String message;
    private Timestamp oTime;
    private String attr1;
    private String attr2;
    private String attr0;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "phone")
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "we_chat")
    public String getWeChat() {
        return weChat;
    }

    public void setWeChat(String weChat) {
        this.weChat = weChat;
    }

    @Basic
    @Column(name = "pay")
    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "result")
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Basic
    @Column(name = "flag")
    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @Basic
    @Column(name = "message")
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Basic
    @Column(name = "o_time")
    public Timestamp getoTime() {
        return oTime;
    }

    public void setoTime(Timestamp oTime) {
        this.oTime = oTime;
    }

    @Basic
    @Column(name = "attr1")
    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    @Basic
    @Column(name = "attr2")
    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    @Basic
    @Column(name = "attr0")
    public String getAttr0() {
        return attr0;
    }

    public void setAttr0(String attr0) {
        this.attr0 = attr0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CashLog cashLog = (CashLog) o;

        if (id != cashLog.id) return false;
        if (openId != null ? !openId.equals(cashLog.openId) : cashLog.openId != null) return false;
        if (phone != null ? !phone.equals(cashLog.phone) : cashLog.phone != null) return false;
        if (weChat != null ? !weChat.equals(cashLog.weChat) : cashLog.weChat != null) return false;
        if (pay != null ? !pay.equals(cashLog.pay) : cashLog.pay != null) return false;
        if (createTime != null ? !createTime.equals(cashLog.createTime) : cashLog.createTime != null) return false;
        if (status != null ? !status.equals(cashLog.status) : cashLog.status != null) return false;
        if (result != null ? !result.equals(cashLog.result) : cashLog.result != null) return false;
        if (flag != null ? !flag.equals(cashLog.flag) : cashLog.flag != null) return false;
        if (message != null ? !message.equals(cashLog.message) : cashLog.message != null) return false;
        if (oTime != null ? !oTime.equals(cashLog.oTime) : cashLog.oTime != null) return false;
        if (attr1 != null ? !attr1.equals(cashLog.attr1) : cashLog.attr1 != null) return false;
        if (attr2 != null ? !attr2.equals(cashLog.attr2) : cashLog.attr2 != null) return false;
        if (attr0 != null ? !attr0.equals(cashLog.attr0) : cashLog.attr0 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result1 = (int) (id ^ (id >>> 32));
        result1 = 31 * result1 + (openId != null ? openId.hashCode() : 0);
        result1 = 31 * result1 + (phone != null ? phone.hashCode() : 0);
        result1 = 31 * result1 + (weChat != null ? weChat.hashCode() : 0);
        result1 = 31 * result1 + (pay != null ? pay.hashCode() : 0);
        result1 = 31 * result1 + (createTime != null ? createTime.hashCode() : 0);
        result1 = 31 * result1 + (status != null ? status.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (flag != null ? flag.hashCode() : 0);
        result1 = 31 * result1 + (message != null ? message.hashCode() : 0);
        result1 = 31 * result1 + (oTime != null ? oTime.hashCode() : 0);
        result1 = 31 * result1 + (attr1 != null ? attr1.hashCode() : 0);
        result1 = 31 * result1 + (attr2 != null ? attr2.hashCode() : 0);
        result1 = 31 * result1 + (attr0 != null ? attr0.hashCode() : 0);
        return result1;
    }
}
