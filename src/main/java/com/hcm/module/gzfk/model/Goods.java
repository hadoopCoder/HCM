package com.hcm.module.gzfk.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 商品行情
 */
@Entity
public class Goods extends KernelModel {
    private long id;
    private String type;
    private String name;
    private String state;
    private BigDecimal buy;
    private BigDecimal sell;
    private BigDecimal more;
    private BigDecimal bull;
    private String status;
    private Timestamp time;
    private Timestamp createTime;

    public Goods(String type, String name) {
        this.type = type;
        this.name = name;
    }

    public Goods(){}

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "state")
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "buy")
    public BigDecimal getBuy() {
        return buy;
    }

    public void setBuy(BigDecimal buy) {
        this.buy = buy;
    }

    @Basic
    @Column(name = "sell")
    public BigDecimal getSell() {
        return sell;
    }

    public void setSell(BigDecimal sell) {
        this.sell = sell;
    }

    @Basic
    @Column(name = "more")
    public BigDecimal getMore() {
        return more;
    }

    public void setMore(BigDecimal more) {
        this.more = more;
    }

    @Basic
    @Column(name = "bull")
    public BigDecimal getBull() {
        return bull;
    }

    public void setBull(BigDecimal bull) {
        this.bull = bull;
    }

    @Basic
    @Column(name = "status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "time")
    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Goods goods = (Goods) o;

        if (id != goods.id) return false;
        if (type != null ? !type.equals(goods.type) : goods.type != null) return false;
        if (name != null ? !name.equals(goods.name) : goods.name != null) return false;
        if (state != null ? !state.equals(goods.state) : goods.state != null) return false;
        if (buy != null ? !buy.equals(goods.buy) : goods.buy != null) return false;
        if (sell != null ? !sell.equals(goods.sell) : goods.sell != null) return false;
        if (more != null ? !more.equals(goods.more) : goods.more != null) return false;
        if (bull != null ? !bull.equals(goods.bull) : goods.bull != null) return false;
        if (status != null ? !status.equals(goods.status) : goods.status != null) return false;
        if (time != null ? !time.equals(goods.time) : goods.time != null) return false;
        if (createTime != null ? !createTime.equals(goods.createTime) : goods.createTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (buy != null ? buy.hashCode() : 0);
        result = 31 * result + (sell != null ? sell.hashCode() : 0);
        result = 31 * result + (more != null ? more.hashCode() : 0);
        result = 31 * result + (bull != null ? bull.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (time != null ? time.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }
}
