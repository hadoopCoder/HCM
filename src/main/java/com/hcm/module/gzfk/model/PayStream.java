package com.hcm.module.gzfk.model;

import com.hcm.kernel.mvc.model.KernelModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 金额变动流水
 */
@Entity
@Table(name = "pay_stream")
public class PayStream extends KernelModel {
    private long id;
    private String openId;
    private String code;
    private String type;
    private BigDecimal pay;
    private BigDecimal beforePay;
    private BigDecimal afterPay;
    private Timestamp createTime;
    private String orderNo;
    private String attr0;
    private String attr1;
    private String attr2;

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "open_id")
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "code")
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "pay")
    public BigDecimal getPay() {
        return pay;
    }

    public void setPay(BigDecimal pay) {
        this.pay = pay;
    }

    @Basic
    @Column(name = "before_pay")
    public BigDecimal getBeforePay() {
        return beforePay;
    }

    public void setBeforePay(BigDecimal beforePay) {
        this.beforePay = beforePay;
    }

    @Basic
    @Column(name = "after_pay")
    public BigDecimal getAfterPay() {
        return afterPay;
    }

    public void setAfterPay(BigDecimal afterPay) {
        this.afterPay = afterPay;
    }

    @Basic
    @Column(name = "create_time")
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "order_no")
    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Basic
    @Column(name = "attr0")
    public String getAttr0() {
        return attr0;
    }

    public void setAttr0(String attr0) {
        this.attr0 = attr0;
    }

    @Basic
    @Column(name = "attr1")
    public String getAttr1() {
        return attr1;
    }

    public void setAttr1(String attr1) {
        this.attr1 = attr1;
    }

    @Basic
    @Column(name = "attr2")
    public String getAttr2() {
        return attr2;
    }

    public void setAttr2(String attr2) {
        this.attr2 = attr2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PayStream payStream = (PayStream) o;

        if (id != payStream.id) return false;
        if (openId != null ? !openId.equals(payStream.openId) : payStream.openId != null) return false;
        if (code != null ? !code.equals(payStream.code) : payStream.code != null) return false;
        if (type != null ? !type.equals(payStream.type) : payStream.type != null) return false;
        if (pay != null ? !pay.equals(payStream.pay) : payStream.pay != null) return false;
        if (beforePay != null ? !beforePay.equals(payStream.beforePay) : payStream.beforePay != null) return false;
        if (afterPay != null ? !afterPay.equals(payStream.afterPay) : payStream.afterPay != null) return false;
        if (createTime != null ? !createTime.equals(payStream.createTime) : payStream.createTime != null) return false;
        if (orderNo != null ? !orderNo.equals(payStream.orderNo) : payStream.orderNo != null) return false;
        if (attr0 != null ? !attr0.equals(payStream.attr0) : payStream.attr0 != null) return false;
        if (attr1 != null ? !attr1.equals(payStream.attr1) : payStream.attr1 != null) return false;
        if (attr2 != null ? !attr2.equals(payStream.attr2) : payStream.attr2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (openId != null ? openId.hashCode() : 0);
        result = 31 * result + (code != null ? code.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (pay != null ? pay.hashCode() : 0);
        result = 31 * result + (beforePay != null ? beforePay.hashCode() : 0);
        result = 31 * result + (afterPay != null ? afterPay.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (orderNo != null ? orderNo.hashCode() : 0);
        result = 31 * result + (attr0 != null ? attr0.hashCode() : 0);
        result = 31 * result + (attr1 != null ? attr1.hashCode() : 0);
        result = 31 * result + (attr2 != null ? attr2.hashCode() : 0);
        return result;
    }
}
