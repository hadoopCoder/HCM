package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.IPayLogDao;
import com.hcm.module.gzfk.model.PayLog;
import org.springframework.stereotype.Repository;

/**
 * 支付历史 dao
 */
@Repository("payLogDao")
public class PayLogDao extends KernelDaoImpl<PayLog> implements IPayLogDao {
}
