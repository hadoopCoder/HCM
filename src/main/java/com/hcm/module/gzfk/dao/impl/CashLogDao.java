package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.ICashLogDao;
import com.hcm.module.gzfk.model.CashLog;
import org.springframework.stereotype.Repository;

/**
 * 提现记录 dao
 */
@Repository("cashLogDao")
public class CashLogDao extends KernelDaoImpl<CashLog> implements ICashLogDao {
}
