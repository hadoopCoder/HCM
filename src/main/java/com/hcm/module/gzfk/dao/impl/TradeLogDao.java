package com.hcm.module.gzfk.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.gzfk.dao.ITradeLogDao;
import com.hcm.module.gzfk.model.TradeLog;
import org.springframework.stereotype.Repository;

/**
 * 交易日志 dao
 *
 */
@Repository("tradeLogDao")
public class TradeLogDao extends KernelDaoImpl<TradeLog> implements ITradeLogDao {
}
