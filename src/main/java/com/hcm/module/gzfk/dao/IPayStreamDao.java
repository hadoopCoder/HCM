package com.hcm.module.gzfk.dao;


import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.PayStream;

/**
 * pay stream dao
 */
public interface IPayStreamDao extends IKernelDao<PayStream> {
}
