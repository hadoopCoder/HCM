package com.hcm.module.gzfk.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.gzfk.model.Trade;

/**
 * trade dao
 */
public interface ITradeDao extends IKernelDao<Trade> {
}
