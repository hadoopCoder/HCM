package com.hcm.module.gzfk.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.kernel.util.Pagination;
import com.hcm.module.gzfk.dao.ITradeLogDao;
import com.hcm.module.gzfk.model.TradeLog;
import com.hcm.module.gzfk.service.ITradeLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 字典操作
 */
@Service("tradeLogService")
public class TradeLogService extends KernelServiceImpl<TradeLog> implements ITradeLogService {
    @Autowired
    ITradeLogDao tradeLogDao;

    @Override
    public List<?> findCount() {
        String sql = "select count(id) cnt,FORMAT(sum(yk)/sum(pay),2)*100 ykv,sum(yk) yk from trade_log";
        return tradeLogDao.findBySql(sql,null,null);
    }

    @Override
    public TradeLog getLast() {
        String sql = "from TradeLog where createTime-0>now()-10 order by createTime desc limit 1";
        //String sql = "select * from trade_log order by create_time desc limit 1";
        List<?> list = tradeLogDao.findByHql(sql, null, null);
        if(list!=null && list.size()>0)return (TradeLog)list.get(0);
        return null;
    }

    @Override
    public List<?> findLastByCode(String code) {
        String hql = "from TradeLog where openId = :openId order by createTime desc";
        Map<String,Object> params = new HashMap<>();
        params.put("openId",code);
        return tradeLogDao.findByHql(hql,new Pagination(1),params);
    }
}
