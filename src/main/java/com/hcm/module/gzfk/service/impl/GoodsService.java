package com.hcm.module.gzfk.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.gzfk.dao.IGoodsDao;
import com.hcm.module.gzfk.model.Goods;
import com.hcm.module.gzfk.service.IGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品操作
 */
@Service("goodsService")
public class GoodsService extends KernelServiceImpl<Goods> implements IGoodsService {

    @Autowired
    IGoodsDao goodsDao;

    @Transactional(readOnly = false)
    public void add(List<Goods> goodsList) {
        for(Goods goods:goodsList){
            goodsDao.insert(goods);
        }
    }

    public List<Map<String, Object>> lastNew(String code) {
        String sql = "select g.id,g.type,g.name,g.state,g.buy,g.sell,more,bull,g.status,g.time,g.create_time," +
                " t.id as tid,open_id,pay,xtype,t.create_time as ttime,gain_per,loss_per,t.price,trade_no from goods g  " +
                " left join trade t on g.type = t.type and t.open_id=:openid order by g.create_time desc limit 3";
        Map<String,Object> param = new HashMap<>();
        param.put("openid",code);
        return (List<Map<String, Object>>) goodsDao.findBySql(sql,null,param);
    }
}
