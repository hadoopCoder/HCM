package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.gzfk.model.CashLog;

/**
 * cash log
 */
public interface ICashLogService extends IKernelService<CashLog> {
}
