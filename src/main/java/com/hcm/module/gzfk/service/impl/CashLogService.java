package com.hcm.module.gzfk.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.gzfk.model.CashLog;
import com.hcm.module.gzfk.service.ICashLogService;
import org.springframework.stereotype.Service;

/**
 * 提现操作
 */
@Service("cashLogService")
public class CashLogService extends KernelServiceImpl<CashLog> implements ICashLogService {
}
