package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.gzfk.model.PayLog;

/**
 * pay log
 */
public interface IPayLogService extends IKernelService<PayLog> {

    void update(String tradeNo, String result);

}
