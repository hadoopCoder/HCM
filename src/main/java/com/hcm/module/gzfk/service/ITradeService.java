package com.hcm.module.gzfk.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.admin.model.User;
import com.hcm.module.gzfk.model.PayStream;
import com.hcm.module.gzfk.model.Trade;

/**
 * trade
 */
public interface ITradeService extends IKernelService<Trade> {

    void trade(Trade trade, User user, PayStream payStream);

}
