package com.hcm.module.gzfk.service.impl;


import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.gzfk.model.PayStream;
import com.hcm.module.gzfk.service.IPayStreamService;
import org.springframework.stereotype.Service;

/**
 * 交易流水操作
 */
@Service("payStreamService")
public class PayStreamService extends KernelServiceImpl<PayStream> implements IPayStreamService {
}
