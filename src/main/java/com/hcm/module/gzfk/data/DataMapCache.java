package com.hcm.module.gzfk.data;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;

/**
 * 主要为了监控定时器的启动情况
 */
public class DataMapCache {

    public static Map<String,Timer> dataMapCache = new HashMap<String,Timer>();

}
