package com.hcm.module.gzfk.data;

import com.hcm.kernel.util.DateUtil;
import com.hcm.kernel.util.Pagination;
import com.hcm.kernel.wechat.MsgAPI;
import com.hcm.module.admin.model.User;
import com.hcm.module.admin.service.IUserService;
import com.hcm.module.gzfk.model.Goods;
import com.hcm.module.gzfk.model.Trade;
import com.hcm.module.gzfk.service.impl.TradeService;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 取数据
 */
public class YKThread extends Thread {

    private List<Goods> list;
    private IUserService userService;
    private TradeService tradeService;

    public YKThread(List<Goods> list,IUserService userService,TradeService tradeService){
        this.list = list;
        this.userService = userService;
        this.tradeService = tradeService;
    }

    public void run() {
        if(list == null) return;
        for(Goods goods:list){
            Trade trade = new Trade();
            trade.setType(goods.getType());
            Pagination pagination = new Pagination(1);
            pagination.setPageSize(100);
            List<Trade> gtList = tradeService.findByEntity(trade, pagination);
            for(Trade t:gtList){
                BigDecimal p = t.getPrice();
                BigDecimal yk;
                if(t.getXtype().equals("多")){
                    BigDecimal s = goods.getSell();
                    yk = s.subtract(p);
                }else{
                    BigDecimal s = goods.getBuy();
                    yk = p.subtract(s);
                }
                if(trade.getType().equals("sliver")){
                    yk = yk.divide(new BigDecimal(10));
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                float per = (yk.divide(t.getPay(),2,BigDecimal.ROUND_HALF_DOWN)).floatValue()*100;
                User user = null;
                if(per >= t.getGainPer() || (Math.abs(per)>=t.getLossPer() && per<0)
                        || (hour<7 && hour>=4)){//到4点后也平仓。
                    if(t.getXtype().equals("多")) {
                        user = userService.close(t.getOpenId(), goods.getType(), t.getPrice().toString(), goods.getSell().toString());
                    }else{
                        user = userService.close(t.getOpenId(), goods.getType(), t.getPrice().toString(), goods.getBuy().toString());
                    }
                }
                if(user!=null) {
                    String date = DateUtil.getDate(new Timestamp(System.currentTimeMillis()), "yyyy年M月d日HH:mm");
                    if (hour < 7 && hour >= 4) {
                        if(t.getXtype().equals("多")) {
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getSell() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】 闭市。");
                        }else{
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getBuy() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】 闭市。");
                        }
                    }
                    if(per >= t.getGainPer()) {
                        if(t.getXtype().equals("多")) {
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getSell() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】 止盈。");
                        }else{
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getBuy() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】 止盈。");
                        }
                    }

                    if(Math.abs(per)>=t.getLossPer() && per<0) {
                        if(t.getXtype().equals("多")) {
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getSell() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】止损。");
                        }else{
                            MsgAPI.send(t.getOpenId(), "您的微交易的持仓已经在" + date + "被系统平仓，【品种】" + t.getName() + "、" +
                                    "【价格】￥ " + goods.getBuy() + "、【盈亏】￥" + user.getAttr0() + "、【平仓后余额】￥ " + user.getPay() + "、【原因】 止损。");
                        }
                    }
                }
            }

        }
    }

}
