package com.hcm.module.gzfk.ctrl.util;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.PayAPI;

/**
 * 封装支付api
 */
public class GPayAPI {

    public static String pay(String random,String openid,String pay){
        return PayAPI.pay("高欧得微盘充值", pay, openid, SystemConfig.getString("appid"),
                SystemConfig.getString("mch_id"), SystemConfig.getString("notify_url"),
                random, SystemConfig.getString("spbill_create_ip"));
    }
}