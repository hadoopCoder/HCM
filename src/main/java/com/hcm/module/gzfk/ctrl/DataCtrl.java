package com.hcm.module.gzfk.ctrl;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.module.admin.service.IUserService;
import com.hcm.module.gzfk.data.DataMapCache;
import com.hcm.module.gzfk.data.FetchDataUtil;
import com.hcm.module.gzfk.data.YKThread;
import com.hcm.module.gzfk.model.Goods;
import com.hcm.module.gzfk.service.impl.GoodsService;
import com.hcm.module.gzfk.service.impl.TradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@Controller
@RequestMapping("/data")
public class DataCtrl {

    @Autowired
    GoodsService goodsService;
    @Autowired
   TradeService tradeService;
    @Autowired
    IUserService userService;

    @RequestMapping("/sync")
    public String interface_(ModelMap mm){
        Timer timer = DataMapCache.dataMapCache.get("timer");
        if(timer != null){
            timer.cancel();
        }

        timer = new Timer();
        DataMapCache.dataMapCache.put("timer", timer);
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                List<Goods> list = FetchDataUtil.getData(SystemConfig.getString("zs.url"), "", goodsService);
                new YKThread(list,userService,tradeService).start();
            }
        }, 0, 5 * 1000);

        mm.put("result","已成功，请点击菜单进行其他操作。");
        return "common/ajax";
    }

}
