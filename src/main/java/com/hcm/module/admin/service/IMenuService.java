package com.hcm.module.admin.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.admin.model.Menu;

import java.util.List;

/**
 * 菜单
 */
public interface IMenuService extends IKernelService<Menu> {

    List<Menu> findByRole(Integer roleId,Integer pid);

}
