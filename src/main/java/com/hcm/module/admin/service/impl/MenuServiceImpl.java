package com.hcm.module.admin.service.impl;

import com.hcm.kernel.mvc.service.impl.KernelServiceImpl;
import com.hcm.module.admin.dao.IMenuDao;
import com.hcm.module.admin.model.Menu;
import com.hcm.module.admin.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("menuService")
public class MenuServiceImpl extends KernelServiceImpl<Menu> implements IMenuService {
    @Autowired
    IMenuDao menuDao;

    @Override
    public List<Menu> findByRole(Integer roleId,Integer pid) {
        String hql = "select menu from Menu menu,RoleMenu rm where rm.roleId = :roleId and rm.menuId = menu.id and menu.pId = :pId order by display,id ";
        Map<String,Object> params = new HashMap<>();
        params.put("roleId",roleId);
        params.put("pId",pid);

        return (List<Menu>) menuDao.findByHql(hql,null,params);
    }
}
