package com.hcm.module.admin.service;

import com.hcm.kernel.mvc.service.IKernelService;
import com.hcm.module.admin.model.Role;

import java.util.List;

public interface IRoleService extends IKernelService<Role> {

    List<Role> findByUser(Integer userId);

}
