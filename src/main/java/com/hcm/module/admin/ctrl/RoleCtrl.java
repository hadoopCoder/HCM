package com.hcm.module.admin.ctrl;

import com.hcm.kernel.mvc.ctrl.KernelCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class RoleCtrl extends KernelCtrl {
}
