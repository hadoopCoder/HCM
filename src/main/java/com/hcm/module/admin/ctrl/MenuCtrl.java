package com.hcm.module.admin.ctrl;

import com.hcm.kernel.mvc.ctrl.KernelCtrl;
import com.hcm.kernel.util.Pagination;
import com.hcm.module.admin.model.Menu;
import com.hcm.module.admin.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin/menu")
public class MenuCtrl extends KernelCtrl {

    @Autowired
    IMenuService menuService;

    @RequestMapping("/list")
    public String list(Pagination pagination,ModelMap mm){
        List<Menu> menuList = menuService.findAll(pagination);
        pagination.setTotal(menuService.countAll());
        mm.put("list", menuList);
        mm.put("page",pagination);
        return "admin/menu/list";
    }

    /**
     * 添加和修改
     * @param menu
     * @param mm
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Menu menu,ModelMap mm){
        menuService.add(menu);
        mm.put("message","{'flag':1,'text':'操作已成功。'}");
        return "common/ajax";
    }

    @RequestMapping("/id")
    public String getById(Menu menu,ModelMap mm){
        menu = menuService.findById(menu.getId());
        mm.put("menu",menu);
        return "admin/menu/edit";
    }

    /**
     * 删除
     * @param menu
     * @param mm
     * @return
     */
    @RequestMapping("/del")
    public String del(Menu menu,ModelMap mm){
        menuService.deleteByIds(String.valueOf(menu.getId()));
        mm.put("message","{'flag':1,'text':'操作已成功。'}");
        return "common/ajax";
    }
}
