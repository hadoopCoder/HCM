package com.hcm.module.admin.ctrl.cache;

import com.hcm.kernel.spring.SpringUtil;
import com.hcm.kernel.util.SystemConfig;
import com.hcm.module.admin.model.Menu;
import com.hcm.module.admin.service.IMenuService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据角色将菜单存贮，如果相对应的角色已经在缓存中存在，那么直接取缓存。
 */
public class MenuCache {

    static IMenuService menuService = SpringUtil.getBean("menuService");

    private static Map<Integer,StringMenu> menuCache = new HashMap<>();

    public static String getStarterMenu(Integer roleId){
        load(roleId);
        return menuCache.get(roleId).getStarter();
    }

    public static String getMainPageMenu(Integer roleId){
        load(roleId);
        return menuCache.get(roleId).getMain();
    }

    private static void load(Integer roleId){
        if(SystemConfig.getBoolean("is_debug")){//如果在开发模式下，将一直加载新菜单从数据库
            loadMenu(roleId);
        } else{
            if(!menuCache.containsKey(roleId)){
                loadMenu(roleId);
            }
        }
    }

    private static void loadMenu(Integer roleId){
        //所有菜单只有两级不存在多级的情况
        StringBuilder starter = new StringBuilder();
        StringBuilder main    = new StringBuilder();
        String first_level = SystemConfig.getString("starter_first_level");
        String second_level = SystemConfig.getString("starter_second_level");
        //String main_first_level = SystemConfig.getString("main_first_level");
        String main_first_level_has_child = SystemConfig.getString("main_first_level_has_child");
        String main_second_level = SystemConfig.getString("main_second_level");

        List<Menu> menuList = menuService.findByRole(roleId,0);
        for(Menu menu:menuList){
            List<Menu> childMenuList = menuService.findByRole(roleId,menu.getId());
            StringBuilder starter_child = new StringBuilder();
            StringBuilder main_child    = new StringBuilder();
            for(Menu childMenu:childMenuList){
                starter_child.append(String.format(second_level,childMenu.getBgcolor(),childMenu.getName(),childMenu.getUrl(),childMenu.getIcon2(),childMenu.getName()));
                main_child.append(String.format(main_second_level,childMenu.getName()));
            }
            starter.append(String.format(first_level,menu.getName(),starter_child));
            main.append(String.format(main_first_level_has_child,menu.getId(),menu.getIcon2(),menu.getName(),menu.getId(),main_child));
        }
        StringMenu stringMenu = new StringMenu(starter.toString(),main.toString());
        menuCache.put(roleId,stringMenu);
    }

    public static class StringMenu{
        private String starter;
        private String main;

        public StringMenu(String starter, String main) {
            this.starter = starter;
            this.main = main;
        }

        public String getMain() {
            return main;
        }

        public String getStarter() {
            return starter;
        }
    }

}
