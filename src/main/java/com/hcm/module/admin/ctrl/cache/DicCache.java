package com.hcm.module.admin.ctrl.cache;

import com.hcm.kernel.spring.SpringUtil;
import com.hcm.module.admin.model.Dic;
import com.hcm.module.admin.service.IDicService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据字典缓存
 */
public class DicCache {

    static IDicService dicService = SpringUtil.getBean("dicService");

    private static Map<String,List<Dic>> dicCache = new HashMap<>();

    public List<Dic> get(String type){
        if(!dicCache.containsKey(type)){
            List<Dic> dicList = dicService.findByType(type);
            dicCache.put(type,dicList);
        }
        return dicCache.get(type);
    }


}
