package com.hcm.module.admin.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.admin.dao.IUserDao;
import com.hcm.module.admin.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends KernelDaoImpl<User> implements IUserDao {
}
