package com.hcm.module.admin.dao;

import com.hcm.kernel.mvc.dao.IKernelDao;
import com.hcm.module.admin.model.Menu;

/**
 * 操作菜单
 */
public interface IMenuDao extends IKernelDao<Menu> {
}
