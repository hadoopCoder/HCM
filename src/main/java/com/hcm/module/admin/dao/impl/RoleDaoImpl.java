package com.hcm.module.admin.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.admin.dao.IRoleDao;
import com.hcm.module.admin.model.Role;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDaoImpl extends KernelDaoImpl<Role> implements IRoleDao {
}
