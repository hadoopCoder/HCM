package com.hcm.module.admin.dao.impl;

import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.admin.dao.IDicDao;
import com.hcm.module.admin.model.Dic;
import org.springframework.stereotype.Repository;

@Repository
public class DicDaoImpl extends KernelDaoImpl<Dic> implements IDicDao {
}
