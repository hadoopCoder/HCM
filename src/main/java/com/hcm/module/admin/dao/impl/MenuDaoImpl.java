package com.hcm.module.admin.dao.impl;


import com.hcm.kernel.mvc.dao.impl.KernelDaoImpl;
import com.hcm.module.admin.dao.IMenuDao;
import com.hcm.module.admin.model.Menu;
import org.springframework.stereotype.Repository;

@Repository
public class MenuDaoImpl extends KernelDaoImpl<Menu> implements IMenuDao {
}
