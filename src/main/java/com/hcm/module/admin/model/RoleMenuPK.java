package com.hcm.module.admin.model;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 角色菜单关联主键
 */
public class RoleMenuPK implements Serializable {
    private Integer roleId;
    private Integer menuId;

    @Column(name = "role_id")
    @Id
    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    @Column(name = "menu_id")
    @Id
    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleMenuPK that = (RoleMenuPK) o;

        if (roleId != that.roleId) return false;
        if (menuId != that.menuId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        Integer result = roleId;
        result = 31 * result + menuId;
        return result;
    }
}
