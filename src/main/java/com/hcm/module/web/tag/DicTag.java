package com.hcm.module.web.tag;

import com.hcm.kernel.util.DicUtil;

/**
 * Created by zhzw on 15/6/27.
 */
public class DicTag {
    public static String findDicName(String type,String code){
        return DicUtil.find(type, code);
    }

}
