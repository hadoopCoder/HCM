package com.hcm.module;

/**
 * 常量
 */
public interface Constants {
    //用户组
    String USER_TYPE_ADMIN = "1";

    //session中user标识
    String SESSION_USER = "user";
    //session 角色标识
    String SESSION_ROLES = "roles";

    //menu
    String STARTER_MENU = "starter_menu";
    String MAIN_MENU    = "main_menu";

}
