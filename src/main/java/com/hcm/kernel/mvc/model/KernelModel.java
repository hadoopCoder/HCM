package com.hcm.kernel.mvc.model;

/**
 * 所有model基类
 */
public class KernelModel {

    private String  outputInfo;//输出信息
    private boolean outputFlag;//输出标识

    public String getOutputInfo() {
        return outputInfo;
    }

    public void setOutputInfo(String outputInfo) {
        this.outputInfo = outputInfo;
    }

    public boolean isOutputFlag() {
        return outputFlag;
    }

    public void setOutputFlag(boolean outputFlag) {
        this.outputFlag = outputFlag;
    }
}
