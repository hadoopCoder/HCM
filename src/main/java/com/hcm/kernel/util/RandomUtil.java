package com.hcm.kernel.util;

import java.util.Random;

/**
 * 生成一个长随机数
 *
 */
public class RandomUtil {

    public static String random(){
        Random random = new Random(System.currentTimeMillis());
        return String.valueOf(Math.abs(random.nextLong()));
    }

}
