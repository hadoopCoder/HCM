package com.hcm.kernel.util;

/**
 * 字符串操作
 */
public class StringUtil {
    public static final char UNDERLINE='_';
    /**
     * 判断字符串string是否为null或者""<br/>
     * 如果为null或者""，则返回true 。否则返回false
     * @param string
     * @return
     */
    public static boolean isBlank(String string){
        return string == null?true:"".equals(string)?true:false;
    }

    /**
     * 如果为null或者""，则返回false 。否则返回true
     * @param string
     * @return
     */
    public static boolean isNotBlank(String string){
        return !isBlank(string);
    }

    public static String getSetMethodName(String param){
        param = underlineToCamel(param);
        StringBuilder setMethodName = new StringBuilder("set");
        setMethodName.append(param.substring(0,1).toUpperCase());
        setMethodName.append(param.substring(1));
        return setMethodName.toString();
    }

    public static String getGetMethodName(String param){
        param = underlineToCamel(param);
        StringBuilder setMethodName = new StringBuilder("get");
        setMethodName.append(param.substring(0,1).toUpperCase());
        setMethodName.append(param.substring(1));
        return setMethodName.toString();
    }

    /**
     * 根据get set方法名获取field名
     * @param getMethodOrSetMethodName
     * @return
     */
    public static String getFieldName(String getMethodOrSetMethodName){
        String field = getMethodOrSetMethodName.replace("get","").replace("set","");
        return field.substring(0,1).toLowerCase()+field.substring(1);
    }

    /**
     * 驼峰命名：将驼峰转成下划线
     * @param param
     * @return
     */
    public static String camelToUnderline(String param){
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        StringBuilder sb=new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (Character.isUpperCase(c)){
                sb.append(UNDERLINE);
                sb.append(Character.toLowerCase(c));
            }else{
                sb.append(c);
            }
        }
        if(sb.toString().startsWith("_")){
            return sb.substring(1);
        }
        return sb.toString();
    }

    /**
     * 将下划线转成驼峰
     * @param param
     * @return
     */
    public static String underlineToCamel(String param){
        if (param==null||"".equals(param.trim())){
            return "";
        }
        int len=param.length();
        StringBuilder sb=new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c=param.charAt(i);
            if (c==UNDERLINE){
                if (++i<len){
                    sb.append(Character.toUpperCase(param.charAt(i)));
                }
            }else{
                sb.append(c);
            }
        }
        return sb.toString();
    }


}
