package com.hcm.kernel.util;

/**
 * 常量
 */
public interface HCMConstants {

    String SESSION_USER = "user";
    String WEBSOCKET_USERNAME = "websocket_username";

}
