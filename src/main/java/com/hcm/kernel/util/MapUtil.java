package com.hcm.kernel.util;


import java.util.HashMap;
import java.util.Map;

/**
 * 简化map操作
 */
public class MapUtil {

    public static Map<String,Object> put(String key,Object value){
        Map<String,Object> map = new HashMap<>();
        map.put(key,value);
        return map;
    }

}
