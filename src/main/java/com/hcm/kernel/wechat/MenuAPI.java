package com.hcm.kernel.wechat;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.util.HttpUtil;
import com.hcm.kernel.wechat.util.TokenCache;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhangzunwei on 15/5/19.
 */
public class MenuAPI {

    public static void createMenu(String menuJson){
        String url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token="+ TokenCache.getAccessToken();
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("createMenu")+"?access_token="+TokenCache.getAccessToken();

        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(menuJson.getBytes());
        HttpUtil.execute(url, byteArrayEntity);
    }

    public static void getMenu(){
        String url = "https://api.weixin.qq.com/cgi-bin/menu/get";
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("getMenu");

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("access_token", TokenCache.getAccessToken()));
        try {
            HttpUtil.execute(url, new UrlEncodedFormEntity(nvps, "utf-8"));
        } catch (UnsupportedEncodingException e) {
            LoggerFactory.getLogger(MenuAPI.class).error("GetMenu error:{}",e.getMessage());
        }
    }

    public static void main(String []args){
        getMenu();
    }
}
