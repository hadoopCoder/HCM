package com.hcm.kernel.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.bean.AccessToken;
import com.hcm.kernel.wechat.util.HttpUtil;
import com.hcm.kernel.wechat.util.TokenCache;

import java.sql.Timestamp;

/**
 *  获取access_token，这里获取的不是用户信息
 */
public class TokenAPI {

    public static AccessToken getToken(String appid,String secret){
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";
        if(!SystemConfig.getBoolean("use_default_interface"))
            url = SystemConfig.getString("getToken");
        url = String.format(url,appid,secret);

        String json = HttpUtil.execute(url, null);
        JSONObject object = JSON.parseObject(json);
        AccessToken token = new AccessToken(object.getString("access_token"),new Timestamp(System.currentTimeMillis()));
        TokenCache.tokenMap.put("token", token);
        return token;
    }

}
