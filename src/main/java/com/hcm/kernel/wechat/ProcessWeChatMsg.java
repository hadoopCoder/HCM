package com.hcm.kernel.wechat;

import com.hcm.kernel.util.StringUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * 处理微信消息事件
 */
public class ProcessWeChatMsg {

    Document document;
    String result;

    public ProcessWeChatMsg(String xml){
        document = parse(xml);
        String event = getEventType();
        String fromUser = getFromUser();
        if(StringUtil.isNotBlank(event)){
            if(event.equalsIgnoreCase(WeChatConstants.EVENT_SUBSCRIBE)){//订阅
                //将基本信息写入user表，并发送消息
//                User user = new User();
//                user.setAccount(fromUser);
//                user.setPwd("");
//                user.setCode(fromUser);
//                user.setRegTime(new Timestamp(System.currentTimeMillis()));
//                user.setLoginNum(2L);
//                IUserService userService = SpringUtil.getBean("userService");
//                userService.add(user);
//
//                //发送欢迎消息
//                MsgAPI.send(fromUser, SystemConfig.getString("welcome"));
            }

            //以下可以添加其他消息

            result = "success";
        }else{
            result = "failure";
        }
    }

    public Document parse(String xml){
        return Jsoup.parse(xml);
    }

    public String getEventType(){
        String event = get("Event");
        return event;
    }

    public String getFromUser(){
        return get("FromUserName");
    }

    public String get(String tagName){
        return document.getElementsByTag(tagName).text();
    }

    public String getResult() {
        return result;
    }
}
