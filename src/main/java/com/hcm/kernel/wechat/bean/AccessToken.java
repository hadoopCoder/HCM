package com.hcm.kernel.wechat.bean;

import java.sql.Timestamp;

/**
 * 微信access_token，因为access_token一天获取的次数有限，
 * 所以为了用户数，这里作了一个缓存，校验token的有效期。
 */
public class AccessToken {

    private String token;
    private Timestamp createTime;

    public AccessToken() {
    }

    public AccessToken(String token, Timestamp createTime) {
        this.token = token;
        this.createTime = createTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public boolean verify(){//超期为true
        return (createTime.getTime()+2*3550*1000)<System.currentTimeMillis();
    }

}
