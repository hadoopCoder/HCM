package com.hcm.kernel.wechat.util;

import com.hcm.kernel.util.SystemConfig;
import com.hcm.kernel.wechat.TokenAPI;
import com.hcm.kernel.wechat.bean.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhangzunwei on 15/5/19.
 */
public class TokenCache {
    private static Logger log = LoggerFactory.getLogger(TokenCache.class);

    public static Map<String,AccessToken> tokenMap = new HashMap<String,AccessToken>();

    public static AccessToken getToken(){
        log.info("getToken........");
        AccessToken token = tokenMap.get("token");
        if(token == null || token.verify()){
            String appid = SystemConfig.getString("appid")==null?"wx349969ca7a967da5":SystemConfig.getString("appid");
            String secret = SystemConfig.getString("secret")==null?"fc8fb5950217ad927d0bf72671d6388c":SystemConfig.getString("secret");
            token = TokenAPI.getToken(appid, secret);
        }
        return token;
    }

    public static String getAccessToken(){
        return getToken().getToken();
    }

}
