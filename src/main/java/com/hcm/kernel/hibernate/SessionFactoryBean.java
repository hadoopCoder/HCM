package com.hcm.kernel.hibernate;

import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

/**
 * hibernate的spring session factory ，可以对hibernate作修改修改操作
 */
public class SessionFactoryBean extends LocalSessionFactoryBean {
}
