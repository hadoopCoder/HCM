package com.hcm.kernel.spring;

import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;

/**
 * spring context loader listener <br/>
 * 基类：{@link ContextLoaderListener}<br/>
 * 初始化spring配置
 */
public class HCMContextLoaderListener extends ContextLoaderListener {

    @Override
    public void contextInitialized(ServletContextEvent event) {
        super.contextInitialized(event);
    }
}
