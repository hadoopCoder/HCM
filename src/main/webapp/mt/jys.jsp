<%--
  Created by IntelliJ IDEA.
  User: zhangzunwei
  Date: 15/5/28
  Time: 09:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>1863微交易-交易所</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
</head>
<body>
<div>
  <div>我们（高欧得）承接的各项交易业务，均是经国家政府批准的交易所，所有的实盘交易均是由以下交易所报价，且在交易所系统实盘/实时撮合完成，在交易所能查实到所有成交的流水单据，确保投资者公平公正地完成交割。
  </div>
  <div>中色金银贸易中心
</div>
  <div>中色金银贸易中心有限公司经国家工商管理总局核准在首都北京成立， 2013年11月于人民大会堂正式揭牌，贸易中心注册资本金为二亿元人民币。营业范围为黄金、白银、有色金属、原油等现货延期交收业务，并为其提供电子商务平台，是为国内唯一一家具有黄金与原油的在线现货交易平台。
</div>
  <div>中色金银贸易中心具有以下特色：</div>
  <li>快：全天候 5天 x 22 小时即时成交</li>
  <li>多：黄金、白银、原油等多品种上线</li>
  <li>好：资金利用率最高仅1%保证金</li>
  <li>省：成本业内最低，节省高达30%</li>
</div>
</body>
</html>
