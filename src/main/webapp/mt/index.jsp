<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <title>1863微交易</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" charset="utf-8">
    <%@include file="init.jsp" %>
</head>
<body>
<div class="modal" id="xx">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            现在为闭市时间，无法进行相关操作。<br>
            请在交易日进行。
        </div>
    </div>
</div>
<div class="header">
    <img src="${user.img}" class="img-circle" style="width:40px;height: 40px;">
    ${user.nickName},余额￥<span id="money">${user.pay}</span>元
    <p class="btn-group header-menu" role="group">
        <button type="button" class="btn btn-default glyphicon glyphicon-plus btn-danger" data-toggle="modal"
                data-target="#despoit">充值
        </button>
        <button type="button" class="btn btn-default glyphicon glyphicon-lock  btn-danger" data-toggle="modal"
                onclick="ruler('/mt/cash.jsp?money=${user.pay}')">提现
        </button>
    </p>
</div>
<div class="clear"></div>

<!--开始banner轮播-->
<div>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/mt/images/banner1.jpg">
            </div>
            <div class="item">
                <img src="/mt/images/banner2.jpg" alt="...白银期货">
            </div>
        </div>
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">前一个</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">下一个</span>
        </a>
    </div>
</div>
<!--结束banner轮播-->

<div class="clear"></div>
<div ng-view></div>
<div class="clear"></div>

<div class="index_table">
    <ul class="nav nav-tabs" id="otherInfoTab">
        <li class="active"><a href="#time_line"> 用户成交记录</a></li>
        <li><a href="#complain">行情分布图 </a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="time_line">
            <table class="table table-striped">
                <tr class="index_table_header">
                    <td>用户</td>
                    <td>品种</td>
                    <td>方向</td>
                    <td>盈亏</td>
                </tr>
                <tbody id="logBody">
                    <c:forEach items="${logList}" var="log">
                        <tr>
                            <td>${log.attr0}</td>
                            <td>${log.name}</td>
                            <td>${log.xtype}</td>
                            <td style="text-align: right;">
                                <c:if test="${log.yk > 0}"><span style="color:red;">${log.yk}</span></c:if>
                                <c:if test="${log.yk == 0}"><span>${log.yk}</span></c:if>
                                <c:if test="${log.yk < 0}"><span style="color:green;">${log.yk}</span></c:if>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
        <div class="tab-pane" id="complain">
            <img id="goldImg" src="http://www.kitco.cn/cn/live_charts/goldcny.gif"
                 style="width:100%;">
    </div>
</div>

<%@include file="modal_cz.jsp"%><!-- 充值 -->
<%@include file="modal_jc.jsp"%><!-- 建仓平仓 -->
<script>
    <c:if test="${not empty msg}">
    alert('${msg}');
    </c:if>
    <c:if test="${not empty mask}">
    $("#xx").slideDown(100);
    </c:if>
    $(function () {
        $('#otherInfoTab a:first').tab('show');//初始化显示哪个tab
        $('#otherInfoTab a').click(function (e) {
            e.preventDefault();//阻止a链接的跳转行为
            $(this).tab('show');//显示当前选中的链接及关联的content
        });
    });
    setInterval("refresh_()",60000);
    function refresh_(){
        $("#goldImg").attr("src","http://www.kitco.cn/cn/live_charts/goldcny.gif?time="+new Date());
    }
    setInterval("refreshLog()",5000);//数据刷新
    function refreshLog(){
        $.get("/g/refreshLog.htm",function(data){
            if(data.indexOf("tr")>0){
                $("#logBody tr:first").before(data);
                $("#logBody tr:last").remove();
            }
        });
    }
    var gold = eval(${gold}),sliver=eval(${sliver}),oil=eval(${oil});

    function ruler(url){
        $("#ruler").load(url).modal();
    }
</script>
</div>
<div id="ruler" class="modal fade" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true"></div>
<p class="text-center navbar-default navbar-fixed-bottom  footer-content"><a href="javascript:ruler('/mt/ruler.jsp');">什么是微交易？</a></p>
<script src="/mt/mt.js"></script>
</body>
</html>