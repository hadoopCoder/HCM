<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 模态框（Modal） -->
<div class="modal fade" id="jiandan_d" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
                data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">
          建【<span id="xxx"></span>】仓
        </h4>
      </div>
      <form action="/g/holding.htm" method="post" id="d_form" class="bs-example bs-example-form">
      <div class="modal-body dialog-caozuo">
          <ul>
            <li>
              <h4><strong id="xxxx">黄金<input type="hidden" name="type" value=""/> </strong><br/>
                <small>品种</small>
              </h4>
            </li>
            <li>
              <h4><strong id="xxxxx">多<input type="hidden" name="xtype" value="多" /></strong><br/>
                <small>方向</small>
              </h4>
            </li>
            <li>
              <h4><strong>1手</strong><br/>
                <small>手数</small>
              </h4>
            </li>
            <li class="currect">
              <h4><strong id="xxxxxx"></strong><br/>
                <small>建仓价</small>
              </h4>
            </li>
            <li>
              <h4><strong id="xxxxxxx"></strong><br/>
                <small>保证金</small>
              </h4>
            </li>
          </ul>
        <div class="clear"></div>
        <div><h3 style="float:left">止盈</h3>

          <div style="width:130px; padding-top:1em; margin-left:1em; float:left;">
              <div class="input-group">
                <span class="input-group-addon" onclick="spiner('-',this);">
                  <a href="###">-</a></span>
                <input type="text" class="form-control" value="100" name="gain" readonly>
                <span class="input-group-addon"  onclick="spiner('+',this);"><a href="###">+</a></span>
              </div>
          </div>
        </div>

        <div class="clear"></div>
        <div><h3 style="float:left">止损</h3>

          <div style="width:130px; padding-top:1em; margin-left:1em;  float:left;">
              <div class="input-group">
                <span class="input-group-addon" onclick="spiner('-',this);"><a href="###">-</a></span>
                <input type="text" class="form-control" value="100" name="loss" readonly>
                <span class="input-group-addon" onclick="spiner('+',this);"><a href="###">+</a></span>
              </div>
          </div>
        </div>
        <div class="clear"></div>

      </div>

      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"
                data-dismiss="modal">关闭
        </button>
        <button type="button" class="btn btn-danger" id="jc_d">
          建仓
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal -->
</div>

<div class="modal fade" id="pc_d" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
                data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">
          平仓
        </h4>
      </div>
      <form action="/g/holding.htm" method="post" id="pc_form" class="bs-example bs-example-form">
        <div class="modal-body dialog-caozuo">
          <ul>
            <li>
              <h4><strong id="y">黄金<input type="hidden" name="type" value="" id="type"/> </strong><br/>
                <small>品种</small>
              </h4>
            </li>
            <li>
              <h4><strong id="yy">多<input type="hidden" name="xtype" value="多" id="xtype"/></strong><br/>
                <small>方向</small>
              </h4>
            </li>
            <li>
              <h4><strong id="yyy">1手</strong><br/>
                <small>手数</small>
              </h4>
            </li>
            <li class="currect">
              <h4><strong id="yyyy"><input type="hidden" name="price" value="" id="buy"/></strong><br/>
                <small>建仓价</small>
              </h4>
            </li>
            <li>
              <h4><strong id="yyyyy"><input type="hidden" name="pay" value="" id="pay"/></strong><br/>
                <small>保证金</small>
              </h4>
            </li>
            <li>
              <h4><strong id="yyyyyy"><input type="hidden" name="sell" value="" id="sell"/></strong><br/>
                <small>平仓价</small>
              </h4>
            </li>
            <li>
              <h4><strong id="yyyyyyy"><input type="hidden" name="yk" value="" id="yk"/>元</strong><br/>
                <small>盈亏</small>
              </h4>
            </li>
          </ul>

          <div class="clear"></div>

        </div>

      </form>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"
                data-dismiss="modal">关闭
        </button>
        <button type="button" class="btn btn-danger" id="pc_b" onclick="pc()">
          平仓
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal -->
</div>