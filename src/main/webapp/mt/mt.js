﻿var money = "5";

$(function(){
    $("#despoit_sel ul li").click(function() {
        money = $(this).find("h4 strong").html();
        if(money.length>5){
            money = "5";
            return;
        }
        $(this).css("background","red").siblings().css("background","#f0f0f0");
        money = money.replace(/[^0-9]/ig,"");
    });

    $("#despoit_but").click(function(){
        disable_button();
        $.post("/weChat/pay.htm",{
            pay:money
        },function(data){
            eval(data);
        });
    });

    $("#jc_d").click(function(){
        disable_button();
        $.post("/g/holding.htm",{
            type:$("#d_form input[name='type']").val(),
            xtype:$("#d_form input[name='xtype']").val(),
            price:$("#d_form input[name='price']").val(),
            gain:$("#d_form input[name='gain']").val(),
            loss:$("#d_form input[name='loss']").val(),
            pay:$("#d_form input[name='pay']").val()
        },function(data){
            alert("操作已成功。");
            window.location="/g/index.htm";
        });
    });

});

function addMore(){
    $("#despoit").html($("#more").html());
}

function pc(){
    disable_button();
    $.post("/g/close.htm",{
        type:$("#pc_d input[name='type']").val(),
        xtype:$("#pc_d input[name='xtype']").val(),
        price:$("#pc_d input[name='price']").val(),
        sell:$("#pc_d input[name='sell']").val()
    },function(data){
        alert("操作已成功。");
        window.location="/g/index.htm";
    });
}

function switch_k(xtype,type,name,sell,pay){
    $("#jiandan_d span[id='xxx']").html(xtype);
    $("#d_form strong[id='xxxx']").html(name+'<input type="hidden" name="type" value="'+type+'" id="type"/> ');
    $("#d_form strong[id='xxxxx']").html(xtype+'<input type="hidden" name="xtype" value="'+xtype+'" id="xtype"/>');
    $("#d_form strong[id='xxxxxx']").html(''+sell+'<input type="hidden" name="price" value="'+sell+'" id="buy"/>');
    $("#d_form strong[id='xxxxxxx']").html(''+pay+'<input type="hidden" name="pay" value="'+pay+'" id="pay"/>');
    $('#jiandan_d').modal();
}

function pc_w(xtype,type,name,price,sell,pay,yk){
    $("#pc_form strong[id='y']").html(name+'<input type="hidden" name="type" value="'+type+'" id="type"/> ');
    $("#pc_form strong[id='yy']").html(xtype+'<input type="hidden" name="xtype" value="'+xtype+'" id="xtype"/>');
    $("#pc_form strong[id='yyyy']").html(price+'<input type="hidden" name="price" value="'+price+'" id="type"/> ');
    $("#pc_form strong[id='yyyyy']").html(pay+'<input type="hidden" name="pay" value="'+pay+'" id="xtype"/>');
    $("#pc_form strong[id='yyyyyy']").html(''+sell+'<input type="hidden" name="sell" value="'+sell+'" id="buy"/>');
    $("#pc_form strong[id='yyyyyyy']").html(''+yk+'<input type="hidden" name="yk" value="'+yk+'" id="pay"/>');
    $('#pc_d').modal();
}

function spiner(type,obj){
    if(type == '-'){
        var x = $(obj).next().val()-20;
        if(x <= 0) x = 20;
        $(obj).next().val(x);
    }else{
        var x = parseInt($(obj).prev().val())+20;
        var max = 100;
        var gain = $(obj).prev().attr("name");
        if(gain == 'gain')max = 200;
        if(x>max) x = max;
        $(obj).prev().val(x);
    }
}

//angular
var routeApp = angular.module("myApp",['ngRoute']).config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/',{
        templateUrl:'/mt/div.jsp',
        controller:'goldController'
    }).when('/s',{
        templateUrl:'/mt/div.jsp',
        controller:'sliverController'
    }).when('/o',{
        templateUrl:'/mt/div.jsp',
        controller:'oilController'
    });
}]);
routeApp.controller("sliverController",function($scope,$timeout){
    $scope.goods = sliver;
    getStatus(gold,sliver,oil,$scope);
    if(sliver.hasOwnProperty('xtype')) $scope.show=true;
    else $scope.show = false;
    var update = function(){
        $.ajax({
            url: '/g/price.htm', success: function (data) {
                sliver = $.parseJSON(data.sliver);
                $scope.goods = sliver;
                getStatus($.parseJSON(data.gold),$.parseJSON(data.sliver),$.parseJSON(data.oil),$scope);
            }, dataType: 'json', async: false
        });
        $timeout(update,5000);
    };
    update()
    $scope.switch_d= function(){
        switch_k('多',sliver.type,sliver.name,sliver.buy,sliver.buy_d);
    };
    $scope.switch_k= function(){
        switch_k('空',sliver.type,sliver.name,sliver.sell,sliver.sell_k);
    };
    $scope.pc=function(){
        if(sliver.xtype && sliver.xtype == '多')
            pc_w(sliver.xtype,sliver.type,sliver.name,sliver.price,sliver.sell,sliver.buy_d,sliver.yk);
        else
            pc_w(sliver.xtype,sliver.type,sliver.name,sliver.price,sliver.buy,sliver.sell_k,sliver.yk);
    };
    $scope.sshow = true;
    $scope.gshow = false;
    $scope.oshow = false;
});
routeApp.controller("goldController",function($scope,$timeout){
    $scope.goods = gold;
    getStatus(gold,sliver,oil,$scope);
    if(gold.hasOwnProperty('xtype')) $scope.show=true;
    else $scope.show = false;
    var update = function() {
        $.ajax({
            url: '/g/price.htm', success: function (data) {
                oil = $.parseJSON(data.oil);
                sliver = $.parseJSON(data.sliver);
                gold = $.parseJSON(data.gold);
                $scope.goods = gold;
                getStatus($.parseJSON(data.gold),$.parseJSON(data.sliver),$.parseJSON(data.oil),$scope);
            }, dataType: 'json', async: false
        });
        $timeout(update, 5000);
    };
    update();

    $scope.switch_d= function(){
        switch_k('多',gold.type,gold.name,gold.buy,gold.buy_d);
    };
    $scope.switch_k= function(){
        switch_k('空',gold.type,gold.name,gold.sell,gold.sell_k);
    };
    $scope.pc=function(){
        if(gold.xtype && gold.xtype == '多')
            pc_w(gold.xtype,gold.type,gold.name,gold.price,gold.sell,gold.buy_d,gold.yk);
        else
            pc_w(gold.xtype,gold.type,gold.name,gold.price,gold.buy,gold.sell_k,gold.yk);
    };
    $scope.sshow = false;
    $scope.gshow = true;
    $scope.oshow = false;
});
routeApp.controller("oilController",function($scope,$timeout){
    $scope.goods = oil;
    getStatus(gold,sliver,oil,$scope);
    if(oil.hasOwnProperty('xtype')) $scope.show=true;
    else $scope.show = false;
    var update = function(){
        $.ajax({
            url: '/g/price.htm', success: function (data) {
                oil = $.parseJSON(data.oil);
                $scope.goods = oil;
                getStatus($.parseJSON(data.gold),$.parseJSON(data.sliver),$.parseJSON(data.oil),$scope);
            }, dataType: 'json', async: false
        });
        $timeout(update,5000);
    };
    update();
    $scope.switch_d= function(){
        switch_k('多',oil.type,oil.name,oil.buy,oil.buy_d);
    };
    $scope.switch_k= function(){
        switch_k('空',oil.type,oil.name,oil.sell,oil.sell_k);
    };
    $scope.pc=function(){
        if(oil.xtype && oil.xtype == '多')
            pc_w(oil.xtype,oil.type,oil.name,oil.price,oil.sell,oil.buy_d,oil.yk);
        else
            pc_w(oil.xtype,oil.type,oil.name,oil.price,oil.buy,oil.sell_k,oil.yk);
    };
    $scope.sshow = false;
    $scope.gshow = false;
    $scope.oshow = true;
});

function tab_board(type){
    if(type == 'g'){
        window.location = "index.htm#/";
    }
    else if(type == 's'){
        window.location = "index.htm#/s";
    }
    else if(type == 'o'){
        window.location = "index.htm#/o";
    }
}

function cash(){
    //addMoreForm
    var name = $('#name').val();
    var mobile = $('#mobile').val();
    if(name == '' && mobile ==''){
        alert('微信号和手机号不可以同时为空，请选填其中一个，谢谢。')
    }else{
        disable_button();
        $('#addMoreForm')[0].submit();
    }
}

function getStatus(gold,sliver,oil,$scope){
    if(oil.hasOwnProperty('xtype')) $scope.cco_show=true;
    else $scope.cco_show = false;
    if(gold.hasOwnProperty('xtype')) $scope.ccg_show=true;
    else $scope.ccg_show = false;
    if(sliver.hasOwnProperty('xtype')) $scope.ccs_show=true;
    else $scope.ccs_show = false;
}

function disable_button(){
    $("button").attr("disabled",true);
}

//强制保留2位小数，如：2，会在2后面补上00.即2.00
function toDecimal2(x) {
    var f = parseFloat(x);
    if (isNaN(f)) {
        return false;
    }
    var f = Math.round(x*100)/100;
    var s = f.toString();
    var rs = s.indexOf('.');
    if (rs < 0) {
        rs = s.length;
        s += '.';
    }
    while (s.length <= rs + 2) {
        s += '0';
    }
    return s;
}    