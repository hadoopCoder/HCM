<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- 模态框（Modal） -->
<div class="modal fade" id="despoit" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close"
                data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">
          充值
        </h4>
      </div>
      <div class="modal-body dialog-caozuo" id="despoit_sel">
        <ul>
          <li style="background: red;">
            <h4><strong>+5元</strong><br/>
              <small>&nbsp;</small>
            </h4>
          </li>
          <li>
            <h4><strong>+10元</strong><br/>
              <small>&nbsp;</small>
            </h4>
          </li>
          <li>
            <h4><strong>+15元</strong><br/>
              <small>&nbsp;</small>
            </h4>
          </li>
          <li>
            <h4><strong>+20元</strong><br/>
              <small>&nbsp;</small>
            </h4>
          </li>
          <li style="width:50%">
            <h4><strong>体验用不到更多哟
                </strong><br/>
              <small>&nbsp;</small>
            </h4>
          </li>
        </ul>
        <div class="clear"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default"
                data-dismiss="modal">关闭
        </button>
        <button type="button" class="btn btn-danger" id="despoit_but">
          充值
        </button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal -->
</div>