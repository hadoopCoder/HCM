<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="boardDiv" data-template>
    <div class="board-brife" ng-show="gshow">
        <div class="board"><h3><strong>{{goods.name}}</strong><br/><small>（实时报价）</small></h3>
            <h4 class="board_buy">买入价<strong style="{{goods.status}}">{{goods.buy}}元/克</strong></h4>
            <h4 class="board_sell">卖出价<strong style="{{goods.status}}">{{goods.sell}}元/克</strong></h4>
        </div>
    </div>
    <div class="board-brife" ng-show="sshow">
        <div class="board"><h3><strong>{{goods.name}}</strong><br/><small>（实时报价）</small></h3>
            <h4 class="board_buy">买入价<strong style="{{goods.status}}">{{goods.buy}}元/千克</strong></h4>
            <h4 class="board_sell">卖出价<strong style="{{goods.status}}">{{goods.sell}}元/千克</strong></h4>
        </div>
    </div>
    <div class="board-brife" ng-show="oshow">
        <div class="board"><h3><strong>{{goods.name}}</strong><br/><small>（实时报价）</small></h3>
            <h4 class="board_buy">买入价<strong style="{{goods.status}}">{{goods.buy}}元/桶</strong></h4>
            <h4 class="board_sell">卖出价<strong style="{{goods.status}}">{{goods.sell}}元/桶</strong></h4>
        </div>
    </div>
    <div class="clear"></div>

    <div class="board-main" ng-show="gshow">
        <div class="board-left" onclick="tab_board('s')">
            <img src="/mt/images/silver.png" class="img-responsive">
            <h3 style="display:none">白银</h3>
            <div class="cc1" ng-show="ccs_show">持仓中</div>
        </div>

        <div class="board-center">
            <img src="/mt/images/gold.png" class="img-responsive">
            <h4>1克 {{goods.name}}</h4>
            <small>保证金 {{goods.buy_d}} 元/手</small>
        </div>

        <div class="board-right" onclick="tab_board('o')">
            <img src="/mt/images/oil.png" class="img-responsive">
            <h3 style="display:none">原油</h3></div>
            <div class="cc2" ng-show="cco_show">持仓中</div>
    </div>

    <div class="board-main" ng-show="sshow">
        <div class="board-left" onclick="tab_board('g')">
            <img src="/mt/images/gold.png" class="img-responsive">
            <h3 style="display:none">黄金</h3>
            <div class="cc1" ng-show="ccg_show">持仓中</div>
        </div>

        <div class="board-center">
            <img src="/mt/images/silver.png" class="img-responsive">
            <h4>0.1千克 {{goods.name}}</h4>
            <small>保证金 {{goods.buy_d}} 元/手</small>
        </div>

        <div class="board-right" onclick="tab_board('o')">
            <img src="/mt/images/oil.png" class="img-responsive">
            <h3 style="display:none">原油</h3></div>
        <div class="cc2" ng-show="cco_show">持仓中</div>
    </div>

    <div class="board-main" ng-show="oshow">
        <div class="board-left" onclick="tab_board('g')">
            <img src="/mt/images/gold.png" class="img-responsive">
            <h3 style="display:none">黄金</h3>
            <div class="cc1" ng-show="ccg_show">持仓中</div>
        </div>

        <div class="board-center">
            <img src="/mt/images/oil.png" class="img-responsive">
            <h4>1桶 {{goods.name}}</h4>
            <small>保证金 {{goods.buy_d}} 元/手</small>
        </div>

        <div class="board-right" onclick="tab_board('s')">
            <img src="/mt/images/silver.png" class="img-responsive">
            <h3 style="display:none">白银</h3></div>
        <div class="cc2" ng-show="ccs_show">持仓中</div>
    </div>

    <div style="clear:both; height:1em;">&nbsp;</div>
    <div class="board_btn" style="border:1px gray solid;">
        <button type="button" class="btn btn-lg btn-danger" style="margin-right:5%;"
                ng-click="switch_d()"  ng-hide="show"
                data-toggle="modal">&nbsp;&nbsp;<span class="glyphicon glyphicon-plus"></span>&nbsp;<b>建【多】单</b>&nbsp;&nbsp;
        </button>
        <button type="button" class="btn btn-lg btn-success" data-toggle="modal"
                ng-click="switch_k()" ng-hide="show">
            &nbsp;&nbsp;<span class="glyphicon glyphicon-plus-sign"></span>&nbsp;建【空】单&nbsp;&nbsp;
        </button>

        <table style="width: 100%;" ng-show="show">
            <tr>
                <td width="25%" style="border-right: 1px gray solid;">建仓价<br/>{{goods.price}}</td>
                <td width="25%" style="border-right: 1px gray solid;">{{goods.xtype}}<br/>方向</td>
                <td width="25%" style="border-right: 1px gray solid;">
                    {{goods.yk}}元<br>
                    当前盈亏
                </td>
                <td width="25%">
                    <button type="button" class="btn btn-lg btn-danger" data-toggle="modal"
                            ng-click="pc()"><span class="glyphicon glyphicon-minus-sign"></span>平仓
                    </button>
                </td>
            </tr>
        </table>
    </div>
</div>