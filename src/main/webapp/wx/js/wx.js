$(".showpro_n_nr").hide();
$(".showpro_n_nr:eq(1)").show();
$(".showpro_n_tab ul > li").each(function (n) {
    $(this).click(function () {
        if (n == 0) {
            $(".showpro_n_tab ul > li:eq(0) img").attr("src", "images/b02.jpg");
            $(".showpro_n_tab ul > li:eq(1) img").attr("src", "images/g01.jpg");
            $(".showpro_n_tab ul > li:eq(2) img").attr("src", "images/y01.jpg");

            $(".showpro_n_tab ul > li.cur1").removeClass("cur1");
            $(".showpro_n_tab ul > li.cur2").removeClass("cur2");
            $(".showpro_n_tab ul > li.cur3").removeClass("cur3");
            $(".showpro_n_tab ul li:eq(1)").addClass("lbg2")
            $(".showpro_n_tab ul li:eq(2)").addClass("lbg3")
            $(this).addClass("cur1");
            $(".showpro_n_nr").hide();
            $(".showpro_n_nr:eq(0)").show();
        }
        if (n == 1) {
            $(".showpro_n_tab ul > li:eq(0) img").attr("src", "images/b01.jpg");
            $(".showpro_n_tab ul > li:eq(1) img").attr("src", "images/g02.jpg");
            $(".showpro_n_tab ul > li:eq(2) img").attr("src", "images/y01.jpg");

            $(".showpro_n_tab ul > li.cur1").removeClass("cur1");
            $(".showpro_n_tab ul > li.cur2").removeClass("cur2");
            $(".showpro_n_tab ul > li.cur3").removeClass("cur3");
            $(".showpro_n_tab ul li:eq(0)").addClass("lbg1")
            $(".showpro_n_tab ul li:eq(2)").addClass("lbg3")
            $(this).addClass("cur2");
            $(".showpro_n_nr").hide();
            $(".showpro_n_nr:eq(1)").show();
        }
        if (n == 2) {
            $(".showpro_n_tab ul > li:eq(0) img").attr("src", "images/b01.jpg");
            $(".showpro_n_tab ul > li:eq(1) img").attr("src", "images/g01.jpg");
            $(".showpro_n_tab ul > li:eq(2) img").attr("src", "images/y02.jpg");

            $(".showpro_n_tab ul > li.cur1").removeClass("cur1");
            $(".showpro_n_tab ul > li.cur2").removeClass("cur2");
            $(".showpro_n_tab ul > li.cur3").removeClass("cur3");
            $(".showpro_n_tab ul li:eq(0)").addClass("lbg1")
            $(".showpro_n_tab ul li:eq(1)").addClass("lbg2")
            $(this).addClass("cur3");
            $(".showpro_n_nr").hide();
            $(".showpro_n_nr:eq(2)").show();
        }

    });
});

$(".cj_n_nr").hide();
$(".cj_n_nr:eq(0)").show();
$(".cj_data ul > li").each(function (n) {
    $(this).click(function () {
        if (n == 0) {
            $(".cj_data ul > li.cur1").removeClass("cur1");
            $(".cj_data ul > li.lbg1").removeClass("lbg1");
            $(".cj_data ul > li.cur2").removeClass("cur2");
            $(".cj_data ul li:eq(1)").addClass("lbg2")
            $(this).addClass("cur1");
            $(".cj_n_nr").hide();
            $(".cj_n_nr:eq(0)").show();
        }
        if (n == 1) {
            $(".cj_data ul > li.cur1").removeClass("cur1");
            $(".cj_data ul > li.cur2").removeClass("cur2");
            $(".cj_data ul > li.lbg2").removeClass("lbg2");
            $(".cj_data ul li:eq(0)").addClass("lbg1")
            $(this).addClass("cur2");
            $(".cj_n_nr").hide();
            $(".cj_n_nr:eq(1)").show();
        }

    });
});

$(document).ready(function () {
    $("#add").click(function () {
        var n = $("#num").val();
        var num = parseInt(n) + 10;
        if (num == 0) {
            alert("cc");
        }
        $("#num").val(num);
    });
    $("#jian").click(function () {
        var n = $("#num").val();
        var num = parseInt(n) - 10;
        if (num == 0) {
            alert("不能为0!");
            return
        }
        $("#num").val(num);
    });

    $("#add2").click(function () {
        var n2 = $("#num2").val();
        var num2 = parseInt(n2) + 10;
        if (num2 == 0) {
            alert("cc");
        }
        $("#num2").val(num2);
    });
    $("#jian2").click(function () {
        var n2 = $("#num2").val();
        var num2 = parseInt(n2) - 10;
        if (num2 == 0) {
            alert("不能为0!");
            return
        }
        $("#num2").val(num2);
    });
});

function openDialog(id,url){
    $(id).load(url).modal({show:true});
}


function loadTemp(id,url,json,xtype) {
    if(xtype)json["xtype"] = xtype;
    var data = $.ajax({url: url, async: false}).responseText;
    var temp = template.compile(data);
    var html = temp(json);
    $(id).html(html).modal({backdrop:"static",show:true});
}

function loadTemp_s(id,url,xtype){
    loadTemp(id,url,sliver,xtype);
}
function loadTemp_g(id,url,xtype){
    loadTemp(id,url,gold,xtype);
}
function loadTemp_o(id,url,xtype){
    loadTemp(id,url,oil,xtype);
}

function disable_button() {//禁用页面中的所有按钮
    $("button").attr("disabled", true);
    $("a").attr("disabled", true);
}

function shake_() {
    shake($("#g_buy"), "red", 2);
    shake($("#g_sell"), "red", 2);
    shake($("#s_buy"), "red", 2);
    shake($("#s_sell"), "red", 2);
    shake($("#o_buy"), "red", 2);
    shake($("#o_sell"), "red", 2);
}

//闪动
function shake(ele, cls, times) {
    var i = 0, t = false, o = ele.attr("class") + " ", c = "", times = times || 2;
    if (t) return;
    t = setInterval(function () {
        i++;
        c = i % 2 ? o + cls : o;
        ele.attr("class", c);
        if (i == 2 * times) {
            clearInterval(t);
            ele.removeClass(cls);
        }
    }, 200);
};