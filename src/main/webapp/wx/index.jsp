<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE HTML>
<html lang="zh-cn">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <%@include file="init.jsp" %>
</head>
<body>
<div class="bodybox">
    <div class="indextop">
        <div class="userpic"><img src="${user.headPic}" class="img-responsive"></div>
        <div class="userinfo">${user.weChatName},余额￥${user.pay}元</div>
        <div class="topright"><a href="#" onclick="openDialog('#dialog','module/pay.jsp')" data-toggle="modal">
                <img src="images/cz.jpg" class="img-responsive"></a>
            <a href="#" onclick="openDialog('#dialog','module/cash.jsp?money=${user.pay}')" data-toggle="modal">
                <img src="images/tx.jpg" class="img-responsive"></a></div>
    </div>
    <div class="modal fade" id="dialog" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true"></div>

    <div class="row banner"><img src="images/banner.jpg" class="img-responsive"></div>
    <!-- head 结束-->

    <div class="showpro_n_tab">
        <ul>
            <li class="lbg1"><img src="images/b01.jpg" class="img-responsive"/></li>
            <li class="cur2"><img src="images/g02.jpg" class="img-responsive"/></li>
            <li class="lbg3"><img src="images/y01.jpg" class="img-responsive"/></li>
        </ul>
    </div>
    <div class="showpro_n_nr">
        <div class="goldbox">
            <div class="goldnotice">白银只需<span id="s_sxf">${sliver['more']}</span>元/手保证金</div>
            <div class="goldshow">
                <div class="goldleft">
                    <div class="mairujia"><span id="s_buy">${sliver['buy']}</span><br>
                        买入价(元/千克)
                    </div>
                </div>
                <div class="goldright">
                    <div class="mairujia"><span id="s_sell">${sliver['sell']}</span><br>
                        卖出价(元/千克)
                    </div>
                </div>
            </div>
            <c:if test="${ empty sliver.xtype}">
                <div class="buybtn">
                    <a href="" class="left" onclick='loadTemp_s("#sjc_dialog","module/sjc_d.jsp","多")'
                       data-toggle="modal"><img src="images/jiancang.jpg" class="img-responsive"/></a>
                    <a href="" class="right" onclick='loadTemp_s("#sjc_dialog","module/sjc_k.jsp","空")'
                       data-toggle="modal"><img src="images/jiancang2.jpg" class="img-responsive"/></a>
                </div>
            </c:if>
            <c:if test="${not empty sliver.xtype}">
                <div class="pingcang">
                    <ul>
                        <li><span>建仓价</span><span>方向</span><span>当前盈亏</span></li>
                        <li>
                            <span><font>${sliver.price}</font></span><span>${sliver.xtype}</span><span><font>${sliver.yk}</font></span>
                        </li>
                    </ul>
                    <c:if test="${sliver.xtype == '多'}">
                        <div class="pc"><a href="#" onclick='loadTemp_s("#sjc_dialog","module/pc_d.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                    <c:if test="${sliver.xtype == '空'}">
                        <div class="pc"><a href="#" onclick='loadTemp_s("#sjc_dialog","module/pc_k.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                </div>
            </c:if>
        </div>

        <div class="modal fade" id="sjc_dialog" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true"></div>
    </div>
    <div class="showpro_n_nr">
        <div class="goldbox">
            <div class="goldnotice">黄金只需<span id="g_sxf">${gold.more}</span>元/手保证金</div>
            <div class="goldshow">
                <div class="goldleft">
                    <div class="mairujia"><span id="g_buy">${gold.buy}</span><br>
                        买入价(元/克)
                    </div>
                </div>
                <div class="goldright">
                    <div class="mairujia"><span id="g_sell">${gold.sell}</span><br>
                        卖出价(元/克)
                    </div>
                </div>
            </div>
            <c:if test="${empty gold.xtype}">
                <div class="buybtn">
                    <a href="#" class="left" onclick='loadTemp_g("#jc_dialog","module/jc_d.jsp","多")'
                       data-toggle="modal"><img src="images/jiancang.jpg" class="img-responsive"/></a>
                    <a href="#" class="right" onclick='loadTemp_g("#jc_dialog","module/jc_k.jsp","空")'
                       data-toggle="modal"><img src="images/jiancang2.jpg" class="img-responsive"/></a>
                </div>
            </c:if>
            <c:if test="${not empty gold.xtype}">
                <div class="pingcang">
                    <ul>
                        <li><span>建仓价</span><span>方向</span><span>当前盈亏</span></li>
                        <li>
                            <span><font>${gold.price}</font></span><span>${gold.xtype}</span><span><font>${gold.yk}</font></span>
                        </li>
                    </ul>
                    <c:if test="${gold.xtype == '多'}">
                        <div class="pc"><a href="#" onclick='loadTemp_g("#jc_dialog","module/pc_d.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                    <c:if test="${gold.xtype == '空'}">
                        <div class="pc"><a href="#" onclick='loadTemp_g("#jc_dialog","module/pc_k.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                </div>
            </c:if>
        </div>

        <div class="modal fade" id="jc_dialog" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel" aria-hidden="true"></div>
    </div>
    <div class="showpro_n_nr">
        <div class="goldbox">
            <div class="goldnotice">原油只需<span id="o_sxf">${oil.more}</span>元/手桶保证金</div>
            <div class="goldshow">
                <div class="goldleft">
                    <div class="mairujia"><span id="o_buy">${oil.buy}</span><br>
                        买入价(元/桶)
                    </div>
                </div>
                <div class="goldright">
                    <div class="mairujia"><span id="o_sell">${oil.sell}</span><br>
                        卖出价(元/桶)
                    </div>
                </div>
            </div>
            <c:if test="${ empty oil.xtype}">
                <div class="buybtn">
                    <a href="" class="left" onclick='loadTemp_o("#ojc_dialog","module/ojc_d.jsp","多")'
                       data-toggle="modal"><img src="images/jiancang.jpg" class="img-responsive"/></a>
                    <a href="" class="right" onclick='loadTemp_o("#ojc_dialog","module/ojc_k.jsp","空")'
                       data-toggle="modal"><img src="images/jiancang2.jpg" class="img-responsive"/></a>
                </div>
            </c:if>
            <c:if test="${not empty oil.xtype}">
                <div class="pingcang">
                    <ul>
                        <li><span>建仓价</span><span>方向</span><span>当前盈亏</span></li>
                        <li>
                            <span><font>${oil.price}</font></span><span>${oil.xtype}</span><span><font>${oil.yk}</font></span>
                        </li>
                    </ul>
                    <c:if test="${oil.xtype == '多'}">
                        <div class="pc"><a href="#" onclick='loadTemp_o("#ojc_dialog","module/pc_d.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                    <c:if test="${oil.xtype == '空'}">
                        <div class="pc"><a href="#" onclick='loadTemp_o("#ojc_dialog","module/pc_k.jsp")'
                                           data-toggle="modal"><img src="images/pingcang.jpg" height="62"/></a></div>
                    </c:if>
                </div>
            </c:if>
            <div class="modal fade" id="ojc_dialog" tabindex="-1" role="dialog"
                 aria-labelledby="myModalLabel" aria-hidden="true"></div>
        </div>
    </div>
    <div class="bline"></div>

    <div class="cj_data">
        <ul>
            <li class="cur1">用户成交数据</li>
            <li class="lbg2">行情走势图</li>
        </ul>
    </div>
    <div class="cj_n_nr">
        <ul>
            <li><span>用户</span><span>品种</span><span>方向</span><span>盈亏</span></li>
        </ul>
        <ul class="yhlist">
            <c:forEach items="${logList}" var="log" varStatus="index">
                <li <c:if test="${index.count%2 == 1}">class="bg"</c:if>>
                    <span>${log.nickName}</span><span>${log.name}</span><span>${log.xtype}</span><span>${log.yk}</span></li>
            </c:forEach>
        </ul>
    </div>
    <div class="cj_n_nr">
        <div class="tab-pane" id="complain">
            <img id="goldImg" src="http://www.kitco.cn/cn/metals/silver/t24_ag_cny_gram_450x275g.gif"
                 style="width:100%;">
        </div>
    </div>
    <script type="text/javascript">
        jQuery(".cj_n_nr").slide({
            mainCell: ".yhlist",
            autoPage: true,
            effect: "top",
            autoPlay: true,
            vis: 6,
            delayTime: 1000
        });
    </script>
    <div class="weijiaoyi"><a href="weijiaoyi.html">什么是微交易？</a></div>
    <div class="tiyanbox">
        <div class="nums_b">
            <span>累计体验</span>
            <div class="nums">${logCount[0]['cnt']}<font>次</font></div>
        </div>

        <div class="nums_b">
            <span>周累计盈利</span>
            <div class="nums">${logCount[0]['yk']}<font>元</font></div>
        </div>

        <div class="nums_b">
            <span>周盈利比</span>
            <div class="nums">${logCount[0].ykv}<font>%</font></div>
        </div>
    </div>

    <div class="footbtn">
        <a href="####"><img src="images/ft_btn1.jpg" class="img-responsive"/></a>
        <a href=""><img src="images/ft_btn2.jpg" class="img-responsive"/></a>
    </div>

    <div class="footbg"></div>

</div>
<script type="text/javascript" src="js/wx.js"></script>
<script>
    var websocket;
    var gold = eval(${gold_});
    var sliver = eval(${sliver_});
    var oil = eval(${oil_});
    websocket = new SockJS("http://godweipan.ijueke.com/webSocket/sockjs/webSocketServer");
    websocket.onmessage = function (event) {
        var json = JSON.parse(event.data);
        $("#g_sxf").html(json.gold.bull);
        $("#g_buy").html(json.gold.buy);
        $("#g_sell").html(json.gold.sell);
        $("#s_sxf").html(json.sliver.bull);
        $("#s_buy").html(json.sliver.buy);
        $("#s_sell").html(json.sliver.sell);
        $("#o_sxf").html(json.oil.bull);
        $("#o_buy").html(json.oil.buy);
        $("#o_sell").html(json.oil.sell);
        shake_();
    };
</script>
</body>
</html>