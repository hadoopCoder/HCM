<%--
  建仓
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header yellow_bg">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">
          +建仓【{{xtype}}单】
        </h4>
      </div>
      <div class="modal-body">
        <form action="holding.htm" method="post" id="d_form" class="bs-example bs-example-form">
        <div class="czje">
          <ul>
            <li><div class="wenhao"><a href=""><img src="/wx/images/weihao.png" /></a></div><span>{{name}}</span>品种</li>
            <li class="chek"><div class="wenhao"><a href=""><img src="/wx/images/weihao.png" /></a></div><span>{{xtype}}</span>方向</li>
            <li><div class="wenhao"><a href=""><img src="/wx/images/weihao.png" /></a></div><span>1手</span>手数</li>
          </ul>
          <ul>
            <li><span class="red">{{buy}}</span>建仓价(元/桶)</li>
            <li><span class="red">{{more}}</span>保证金(元)
                <input type="hidden" name="pay" value="{{more}}">
                <input type="hidden" name="price" value="{{buy}}">
                <input type="hidden" name="type" value="oil">
                <input type="hidden" name="xtype" value="{{xtype}}">
            </li>
          </ul>

        </div>
        <div class="jiancang_box">
          <div class="jcleft">
            <div class="zhiying"><p>止盈<a href="#"><img src="/wx/images/weihao.png" /></a></p>
              <p> <input type="button" id="add" value="+" />
                <input type="text" id="num" value="100" name="gain"/>
                <input type="button" id="jian" value="-" /></p>
            </div>
            <div class="zhisun"><p>止损<a href="#"><img src="/wx/images/weihao.png" /></a></p><p> <input type="button" id="add2" value="+" />
              <input type="text" id="num2" value="100" name="loss"/>
              <input type="button" id="jian2" value="-" /></p></div>
          </div>
          <div class="jcright">
            <image class="czbtn"  onclick="submit();" src="/wx/images/jcbtn.jpg"/>
          </div>
        </div>
        </form>
      </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
<script>
    var numx = 0;
    function submit(){
        if(numx == 0){
            numx ++;
            disable_button();
            $('#d_form')[0].submit();
        }else{
            alert("等待页面处理中，请不要重复操作好不好。");
        }
    }
    $(function(){
        $("#add").click(function () {
            var n = $("#num").val();
            var num = parseInt(n) + 20;
            if (num > 200) {
                num = 200;
            }
            $("#num").val(num);
        });
        $("#jian").click(function () {
            var n = $("#num").val();
            var num = parseInt(n) - 20;
            if (num < 0) {
                num = 0;
            }
            $("#num").val(num);
        });

        $("#add2").click(function () {
            var n2 = $("#num2").val();
            var num2 = parseInt(n2) + 20;
            if (num2 > 200) {
                num2 = 200;
            }
            $("#num2").val(num2);
        });
        $("#jian2").click(function () {
            var n2 = $("#num2").val();
            var num2 = parseInt(n2) - 20;
            if (num2 < 0) {
                num2 = 0;
            }
            $("#num2").val(num2);
        });

    });
</script>