<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header yellow_bg">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h4 class="modal-title" id="myModalLabel">
        提现申请
      </h4>
    </div>
    <div class="modal-body">
      <form action="saveMore.htm" method="post" id="addMoreForm" class="bs-example bs-example-form">
        <div style="color:red;font-size:14pt;">
          请填写你的微信号或者手机号码后提现。方便客服美眉联系你，以保提现成功！
        </div>
        <div class="clear"></div>
        <input type="hidden" name="token" value="${param.token}" />
        微信号码<input name="name" id="name" placeholder="微信号"><br>
        手机号码<input name="mobile" id="mobile" placeholder="手机号码"><br>
        提现金额<input name="pay" placeholder="提现金额" value="${param.money}" required readonly>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default"
              data-dismiss="modal">关闭
      </button>
      <button type="button" class="btn btn-danger" onclick="disable_button();$('#addMoreForm')[0].submit();">
        确认提现
      </button>
    </div>
  </div><!-- /.modal-content -->
</div><!-- /.modal -->