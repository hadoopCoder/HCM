<%@ page import="java.io.PrintWriter" %>
<%@ page contentType="text/html;charset=UTF-8" isErrorPage="true" language="java" %>
<%@include file="init.jsp" %>
<%
    PrintWriter writer = response.getWriter();
    writer.print("发生系统错误，请确认<br/>");
    writer.print("<pre>");
    writer.print(exception.getMessage()+"<br/>");
    exception.printStackTrace(writer);
    writer.print("</pre>");
%>