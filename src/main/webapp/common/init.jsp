<%
    String base = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";
%>
<base href="<%=base%>"/>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="description" content="">
<meta name="keywords" content="">
<link rel='shortcut icon' type='image/x-icon' href='static/favicon.ico' />
<!-- css -->
<link href="static/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/>
<link href="static/metro/css/metro.css" type="text/css" rel="stylesheet"/>
<link href="static/metro/css/metro-icons.css" type="text/css" rel="stylesheet"/>
<link href="static/font-icon/css/font-awesome.css" type="text/css" rel="stylesheet"/>
<link href="static/custom/css.css" type="text/css" rel="stylesheet"/>
<!-- js -->
<script src="static/jquery/jquery-1.11.3.min.js"></script>
<!--[if lt IE 9]>
<script src="static/jquery/html5shiv.js"></script>
<script src="static/jquery/respond.min.js"></script>
<![endif]-->
<script src="static/metro/js/metro.js"></script>
<script src="static/jquery/jquery.twbsPagination.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>