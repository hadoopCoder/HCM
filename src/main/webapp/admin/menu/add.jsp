<%@ page contentType="text/html;charset=UTF-8" %>
<div>
    <form>
        <div class="panel panel-primary">
            <div class="panel-heading">添加菜单</div>
            <div class="panel-body">
                <div class="grid">
                    <div class="row">
                        <div class="cell">
                            <label>用户名</label>
                            <div class="input-control text full-size">
                                <input type="text" name="account" placeholder="请输入用户名">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">
                            <label>密码</label>

                            <div class="input-control text full-size">
                                <input type="password" name="pwd" placeholder="请输入密码">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">
                            <label class="input-control checkbox">
                                <input type="checkbox" checked>
                                <span class="check"></span>
                                <span class="caption">记住用户名</span>
                            </label>
                            <label class="input-control get-pass">
                                <a href=""> 找回密码？</a>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="cell">
                            <div class="input-control text full-size">
                                <input type="submit" value="登录"
                                       class="button info block-shadow-info text-shadow">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </form>
</div>