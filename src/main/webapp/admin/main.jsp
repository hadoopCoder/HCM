<%@page pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head lang="en">
    <title>管理系统-[]</title>
    <%@include file="init.jsp" %>
</head>
<body>
<div class="app-bar" data-role="appbar">
    <ul class="app-bar-menu">
        <li>
            <a class="">管理系统</a>
        </li>
    </ul>
</div>
<div style="padding-left:225px;">
    <div class="side_nav">
        <ul class="v-menu subdown">
            ${main_menu}
        </ul>
    </div>
    <div class="container-fruit page-content" id="mainContent">
        <script>ajaxLoad('${param.name}', 'admin/menu/list.htm?page=1');</script>
    </div>
</div>
</body>
</html>